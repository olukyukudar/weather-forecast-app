import requests
import json

place = input ("Place: ")
place_lower = place.lower()

api = (f"http://www.wttr.in/{place_lower}?format=j1")

response = requests.get(api)

#print (response)

data = response.text

parsed = json.loads(data)


#print (json.dumps(parsed, indent = 4))

c = parsed["current_condition"][0]["FeelsLikeC"]
f = parsed["current_condition"][0]["FeelsLikeF"]
humidity = parsed["current_condition"][0]["humidity"]
cloudCover = parsed["current_condition"][0]["cloudcover"]
value = parsed["current_condition"][0]["weatherDesc"][0]["value"]
chanceOfRain =parsed["weather"][0]["hourly"][0]["chanceofrain"]





print (f"\nToday is {value} in {place_lower.title()}.\nTemperature is {c}C/{f}F.\nHumidity is %{humidity}\nCloud Cover is %{cloudCover}\nChance of rain is %{chanceOfRain}\n")

print ("----------------------------------------")
